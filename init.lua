-- Copyright (c) 2016, 2017, 2018 Gabriel Pérez-Cerezo, licensed under WTFPL.
-- Wire Transfers (c) 2018 Hans von Smacker
-- Large ATMs (C) 2017 Hans von Smacker

atm = {}
atm.balance = {}
atm.pending_transfers = {}
atm.completed_transactions = {}
atm.pth = minetest.get_worldpath().."/atm_accounts"
atm.pth_wt = minetest.get_worldpath().."/atm_wt_transactions"
local modpath = minetest.get_modpath("atm")
atm.startbalance = 0
function atm.ensure_init(name)
   -- Ensure the atm account for the placer specified by name exists
   atm.readaccounts()
   if not atm.balance[name] then
      atm.balance[name] = atm.startbalance
   end
end
local function atmlog(message)
	minetest.log("action", "[ATM] "..message)
end

function atm.pay(name, amount, source)
	if amount < 0 then
		return false -- This should be done using withdraw function
	end
	atm.ensure_init(name)
	atm.balance[name] = math.floor(atm.balance[name]+amount)
	atm.saveaccounts()
	return true
end

function atm.withdraw(name, amount, source)
	if amount > 0 then
		return false -- This should be done using pay function
	end
	atm.ensure_init(name)
	if amount > atm.balance[name] then
		atmlog("Player "..name.." tried to withdraw "..amount.." - Not enough money! Reason: "..source)
		return false
	end
	atm.balance[name] = math.floor(atm.balance[name]+amount)
	atm.saveaccounts()
	return true
end

function atm.get_balance(name)
	atm.ensure_init(name)
	return atm.balance[name]
end

function atm.showform (player)
	atm.ensure_init(player:get_player_name())
	local formspec =
	"size[11.75,8.5]"..
	default.gui_bg..
	default.gui_bg_img..
	default.gui_slots..
	"label[2,0.5;Money input]" ..
	"label[8.25,0.5;Money output]" ..
	"label[2.5,0.15;Your account balance: $".. atm.balance[player:get_player_name()].. "]" ..
	"button_exit[5.4,2.75;1,2;Quit;Quit]" ..
	-- 1
	"item_image_button[0.0,1;1,1;".. "currency:valgeld" ..";i1;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[0.9,1;1,1;".. "currency:valgeld_4" ..";i4;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[1.8,1;1,1;".. "currency:valgeld_16" ..";i16;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[2.7,1;1,1;".. "currency:valgeld_64" ..";i64;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[3.6,1;1,1;".. "currency:valgeld_256" ..";i256;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[4.5,1;1,1;".. "currency:valgeld_1024" ..";i1024;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[6.3,1;1,1;".. "currency:valgeld" ..";i-1;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[7.2,1;1,1;".. "currency:valgeld_4" ..";i-4;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[8.1,1;1,1;".. "currency:valgeld_16" ..";i-16;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[9.0,1;1,1;".. "currency:valgeld_64" ..";i-64;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[9.9,1;1,1;".. "currency:valgeld_256" ..";i-256;\n\n\b\b\b\b\b" .. "1" .."]" ..
	"item_image_button[10.8,1;1,1;".. "currency:valgeld_1024" ..";i-1024;\n\n\b\b\b\b\b" .. "1" .."]" ..
	-- 2
	"item_image_button[0.0,2;1,1;".. "currency:valgeld" ..";t2;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[0.9,2;1,1;".. "currency:valgeld_4" ..";t8;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[1.8,2;1,1;".. "currency:valgeld_16" ..";t32;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[2.7,2;1,1;".. "currency:valgeld_64" ..";t128;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[3.6,2;1,1;".. "currency:valgeld_256" ..";t512;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[4.5,2;1,1;".. "currency:valgeld_1024" ..";t2048;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[6.3,2;1,1;".. "currency:valgeld" ..";t-2;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[7.2,2;1,1;".. "currency:valgeld_4" ..";t-8;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[8.1,2;1,1;".. "currency:valgeld_16" ..";t-32;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[9.0,2;1,1;".. "currency:valgeld_64" ..";t-128;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[9.9,2;1,1;".. "currency:valgeld_256" ..";t-512;\n\n\b\b\b\b\b" .. "2" .."]" ..
	"item_image_button[10.8,2;1,1;".. "currency:valgeld_1024" ..";t-2048;\n\n\b\b\b\b\b" .. "2" .."]" ..
	-- 3
	"item_image_button[0.0,3;1,1;".. "currency:valgeld" ..";c3;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[0.9,3;1,1;".. "currency:valgeld_4" ..";c12;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[1.8,3;1,1;".. "currency:valgeld_16" ..";c48;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[2.7,3;1,1;".. "currency:valgeld_64" ..";c192;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[3.6,3;1,1;".. "currency:valgeld_256" ..";c768;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[4.5,3;1,1;".. "currency:valgeld_1024" ..";c3072;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[6.3,3;1,1;".. "currency:valgeld" ..";c-3;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[7.2,3;1,1;".. "currency:valgeld_4" ..";c-12;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[8.1,3;1,1;".. "currency:valgeld_16" ..";c-48;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[9.0,3;1,1;".. "currency:valgeld_64" ..";c-192;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[9.9,3;1,1;".. "currency:valgeld_256" ..";c-768;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"item_image_button[10.8,3;1,1;".. "currency:valgeld_1024" ..";c-3072;\n\n\b\b\b\b\b" .. "3" .."]" ..
	"list[current_player;main;0,4.25;8,1;]"..
	"list[current_player;main;0,5.5;8,3;8]"..
	"listring[]"..
	default.get_hotbar_bg(0, 4.25)
	minetest.after((0.1), function(gui)
			return minetest.show_formspec(player:get_player_name(), "atm.form", gui)
		end, formspec)
end



-- wire transfer interface

function atm.showform_wt (player)
	atm.ensure_init(player:get_player_name())
	local formspec =
	"size[8,6]"..
	default.gui_bg..
	default.gui_bg_img..
	default.gui_slots..
	"button[5.75,0;2,1;transactions;Transactions >]" ..
	"label[2.5,0;Wire Transfer Terminal]" ..
	"label[2,0.5;Your account balance: $".. atm.balance[player:get_player_name()].. "]" ..
	"field[0.5,1.5;5,1;dstn;Recepient:;]"..
	"field[6,1.5;2,1;amnt;Amount:;]"..
	"field[0.5,3;7.5,1;desc;Description:;]"..
	"button_exit[0.2,5;1,1;Quit;Quit]" ..
	"button[4.7,5;3,1;pay;Complete the payment]"
	minetest.after((0.1), function(gui)
			return minetest.show_formspec(player:get_player_name(), "atm.form.wt", gui)
		end, formspec)
end

function atm.showform_wtconf (player, dstn, amnt, desc)
	atm.ensure_init(player:get_player_name())
	local formspec =
	"size[8,6]"..
	default.gui_bg..
	default.gui_bg_img..
	default.gui_slots..
	"label[2.5,0;Wire Transfer Terminal]" ..
	"label[2,0.5;Your account balance: $".. atm.balance[player:get_player_name()].. "]" ..
	"label[2.5,1;TRANSACTION SUMMARY:]"..
	"label[0.5,1.5;Recepient: " .. dstn .. "]"..
	"label[0.5,2;Amount: " .. amnt .. "]"..
	"label[0.5,2.5;Description: " .. desc .. "]"..
	"button_exit[0.2,5;1,1;Quit;Quit]" ..
	"button[4.7,5;3,1;cnfrm;Confirm transfer]"
	minetest.after((0.1), function(gui)
			return minetest.show_formspec(player:get_player_name(), "atm.form.wtc", gui)
		end, formspec)
end

function atm.showform_wtlist (player, tlist)
	atm.ensure_init(player:get_player_name())

	local textlist = ''

	if not tlist then
		textlist = "no transactions registered\n"
	else
		for i,entry in ipairs(tlist) do
			textlist = textlist .. entry.date .. " $" .. entry.sum .. " from " .. entry.from .. ": " .. entry.desc .. "\n"
		end
	end

	local formspec =
	"size[8,6]"..
	default.gui_bg..
	default.gui_bg_img..
	default.gui_slots..
	"button[5.75,0;2,1;transfer;< Transfer money]" ..
	"label[2.5,0;Wire Transfer Terminal]" ..
	"label[2,0.5;Your account balance: $".. atm.balance[player:get_player_name()].. "]" ..
	"textarea[0.5,1.25;7.5,4;hst;Transaction list;" .. textlist .. "]" ..
	"button_exit[0.2,5;1,1;Quit;Quit]" ..
	"button[4.7,5;3,1;clr;Clear transactions]"
	minetest.after((0.1), function(gui)
			return minetest.show_formspec(player:get_player_name(), "atm.form.wtl", gui)
		end, formspec)
end


-- banking accounts storage

function atm.readaccounts ()
	local b = atm.balance
	local file = io.open(atm.pth, "r")
	if file then
		repeat
			local balance = file:read("*n")
			if balance == nil then
				break
			end
			local name = file:read("*l")
			b[name:sub(2)] = balance
		until file:read(0) == nil
		io.close(file)
	else
		b = {}
	end
end

function atm.saveaccounts()
	if not atm.balance then
		return
	end
	local data = {}
	for k, v in pairs(atm.balance) do
		table.insert(data, string.format("%d %s\n", v, k))
	end

	local output = io.open(atm.pth, "w")
	output:write(table.concat(data))
	io.close(output)
end


-- wire transfer data storage

function atm.read_transactions()
	local file = io.open(atm.pth_wt, "r")
	if file then
		local data = file:read("*all")
		atm.completed_transactions = minetest.deserialize(data)
	end
end

function atm.write_transactions()
	if not atm.completed_transactions then
		return
	end
	local file = io.open(atm.pth_wt, "w")
	local data = minetest.serialize(atm.completed_transactions)
	file:write(data)
	io.close(file)
end



minetest.register_on_joinplayer(function(player)
	atm.readaccounts()
end)


-- ATM node definitions

minetest.register_node("atm:atm", {
	description = "ATM",
	tiles = {
		"atm_top.png", "atm_top.png",
		"atm_side.png", "atm_side.png",
		"atm_side.png", "atm_front.png"
	},
	paramtype2 = "facedir",
	groups = {cracky=2, bank_equipment = 1},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),

	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		atm.showform(player)
	end,
})

-- Wire transfer terminal node

minetest.register_node("atm:wtt", {
	description = "Wire Transfer Terminal",
	tiles = {
		"atm_top.png", "atm_top.png",
		"atm_side_wt.png", "atm_side_wt.png",
		"atm_side_wt.png", "atm_front_wt.png"
	},
	paramtype2 = "facedir",
	groups = {cracky=2, bank_equipment = 1},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),

	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		atm.showform_wt(player)
	end,
})


-- Check the form

minetest.register_on_player_receive_fields(function(player, form, pressed)

	-- ATMs
	if form == "atm.form" then
		local n = player:get_player_name()
		local transaction = { amount = 0, denomination = 0, count = 0 }
		local pinv=player:get_inventory()

		-- single note transactions
		for _,i in pairs({1, 4, 16, 64, 256, 1024, -1, -4, -16, -64, -256,
			-1024}) do

			if pressed["i"..i] then
				transaction.amount = i
				transaction.denomination = '_' .. math.abs(i)
				if transaction.denomination == '_1' then
					transaction.denomination = ''
				end
				transaction.count = ' ' .. 1
				break
			end
		end

		-- 2x banknote transactions
		for _,t in pairs({2, 8, 32, 128, 512, 2048, -2, -8, -32, -128, -512,
			-2048}) do

			if pressed["t"..t] then
				transaction.amount = t
				transaction.denomination = '_' .. math.abs(t/2)
				if transaction.denomination == '_1' then
					transaction.denomination = ''
				end
				transaction.count = ' ' .. 2
				break
			end
		end

		-- 3x banknote transactions
		for _,c in pairs({3, 12, 48, 192, 768, 3072, -3, -12, -48, -192,
			-768, -3072}) do

			if pressed["c"..c] then
				transaction.amount = c
				transaction.denomination = '_' .. math.abs(c/3)
				if transaction.denomination == '_1' then
					transaction.denomination = ''
				end
				transaction.count = ' ' .. 3
				break
			end
		end

		if (atm.balance[n] + transaction.amount) < 0 then
			minetest.chat_send_player(n, "Not enough money in your account")
			transaction.amount = 0
		end

		local item = "currency:valgeld" .. transaction.denomination .. transaction.count

		if transaction.amount < 0 then
			if pinv:room_for_item("main", item) then
				pinv:add_item("main", item)
				atm.balance[n] = atm.balance[n] + transaction.amount
			else
				minetest.chat_send_player(n, "Not enough room in your inventory")
			end

		elseif transaction.amount > 0 then
			if pinv:contains_item("main", item) then
				pinv:remove_item("main", item)
				atm.balance[n] = atm.balance[n] + transaction.amount
			else
				minetest.chat_send_player(n, "Not enough money in your inventory")
			end
		end

		atm.saveaccounts()

		if not pressed.Quit and not pressed.quit then
			atm.showform(player)
		end

	-- Wire transfer terminals
	elseif form == "atm.form.wt" or form == "atm.form.wtc" or form == "atm.form.wtl" then

		local n = player:get_player_name()

		if not pressed.Quit and not pressed.quit then
			if form == "atm.form.wt" and pressed.transactions then
				-- transaction list (can be edited in the form, but than means nothing)
				atm.read_transactions()
				atm.showform_wtlist(player, atm.completed_transactions[n])
			elseif form == "atm.form.wtl" and pressed.transfer then
				atm.showform_wt(player)
			elseif form == "atm.form.wtl" and pressed.clr then
				-- clear all transactions in the player's list
				atm.read_transactions()
				atm.completed_transactions[n] = nil
				atm.write_transactions()
				minetest.chat_send_player(n, "Your transaction history has been cleared")
				atm.showform_wtlist(player, atm.completed_transactions[n])
			elseif form == "atm.form.wt" and pressed.pay then

				-- perform the checks of validity for wire transfer order
				-- if passed, store the data in a temporary table and show confirmation window
				if not atm.balance[pressed.dstn] then
					minetest.chat_send_player(n, "The recepient <" .. pressed.dstn .. "> is not registered in the banking system, aborting")
					atm.showform_wt(player)
				elseif not string.match(pressed.amnt, '^[0-9]+$') then
					minetest.chat_send_player(n, "Invalid amount <" .. pressed.amnt .. "> : must be an integer number, aborting")
					atm.showform_wt(player)
				elseif atm.balance[n] < tonumber(pressed.amnt) then
					minetest.chat_send_player(n, "Your account does not have enough funds to complete this transfer, aborting")
					atm.showform_wt(player)
				else
					atm.pending_transfers[n] = {to = pressed.dstn, sum = tonumber(pressed.amnt), desc = pressed.desc}
					atm.showform_wtconf(player, pressed.dstn, pressed.amnt, pressed.desc)
				end

			elseif form == "atm.form.wtc" then
				-- transaction processing
				atm.read_transactions()
				local t = atm.pending_transfers[n]
				if not atm.completed_transactions[t.to] then
					atm.completed_transactions[t.to] = {}
				end

				if atm.balance[n] < t.sum then
					-- you can never be too paranoid about the funds availaible
				   minetest.chat_send_player(n, "Your account does not have enough funds to complete this transfer, aborting")
				   if not t.extern then
				      atm.showform_wt(player)
				   else
				      minetest.close_formspec(n, "atm.form.wtc")
				   end
				   return
				end

				table.insert(atm.completed_transactions[t.to], {date=os.date("%Y-%m-%d"), from=n, sum=t.sum, desc=t.desc})
				atm.balance[n] = atm.balance[n] - t.sum
				atm.balance[t.to] = atm.balance[t.to] + t.sum
				atm.write_transactions()
				atm.saveaccounts()
				minetest.chat_send_player(n, "Payment of " .. t.sum .. " to " .. t.to .. " completed")
				minetest.chat_send_player(n, n .. ", thank you for choosing the Wire Transfer system")
				if t.callback then -- run callbacks from mods
				   t.callback(t)
				end
				if t.extern == true then -- Transfer was initiated by mod
				   atm.pending_transfers[n] = nil
				   minetest.close_formspec(n, "atm.form.wtc")
				   return
				end
				atm.pending_transfers[n] = nil
				atm.showform_wt(player)
			end
		else
			-- clear the pending transaction of the player, just in case
			if atm.pending_transfers[n] then
				atm.pending_transfers[n] = nil
			end
		end

	end

end)

local cheaper_part = "default:copper_ingot"

if minetest.get_modpath("mesecons") then
	cheaper_part = "mesecons:wire_00000000_off"
end

minetest.register_craft({
	output = "atm:atm",
	recipe = {
		{"default:steel_ingot", "default:mese_crystal", "default:steel_ingot"},
		{"default:glass", "currency:valgeld_16", "default:steel_ingot"},
		{"default:steel_ingot", "default:mese_crystal", "default:steel_ingot"}
	}
})

minetest.register_craft({
	output = "atm:wtt",
	recipe = {
		{"default:steel_ingot", "default:mese_crystal", "default:steel_ingot"},
		{"default:glass", cheaper_part, "default:steel_ingot"},
		{"default:steel_ingot", "default:mese_crystal", "default:steel_ingot"}
	}
})
